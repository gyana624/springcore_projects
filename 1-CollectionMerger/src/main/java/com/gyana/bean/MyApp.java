package com.gyana.bean;



import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyApp {
	public static void main(String[] args) {
		
		ApplicationContext context=new ClassPathXmlApplicationContext("Beans.xml");
		
		Meeting scrum = context.getBean("sm", Meeting.class);
		System.out.println(scrum);//steve, ashok, anil, charan

		Meeting functional = context.getBean("fm", Meeting.class);
		System.out.println(functional);//steve, ashok, anil, charan, charles, Orlen
	}

}
