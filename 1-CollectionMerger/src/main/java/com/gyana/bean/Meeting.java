package com.gyana.bean;

import java.util.List;

import lombok.Data;
import lombok.ToString;
@Data
@ToString
public class Meeting {
	
private String meetingName;
private List<String> participants;

public String getMeetingName() {
	return meetingName;
}
public void setMeetingName(String meetingName) {
	this.meetingName = meetingName;
}
public List<String> getParticipants() {
	return participants;
}
public void setParticipants(List<String> participants) {
	this.participants = participants;
}
@Override
public String toString() {
	return "Meeting [meetingName=" + meetingName + ", participants=" + participants + "]";
}


}
